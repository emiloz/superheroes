import { Heroe, HeroeImages } from '../api/interfaces';

const transformData = async (data) => {
  return data.map((hero) => {
    const {
      id,
      name,
      powerstats: {
        combat,
        durability,
        intelligence,
        power,
        speed,
        strength
      },
      biography: {
        fullName,
      },
      images: {
        md,
        sm,
      },
    } = hero;

    const sumPower = combat + durability + intelligence + power + speed + strength;
    const powerstats = +((((sumPower) / 600) * 100).toFixed(1)) / 10;

    const heroeImages: HeroeImages = {
      md,
      sm,
    };

    const heroe: Heroe = {
      id,
      name,
      powerstats: +powerstats.toFixed(1),
      fullName,
      images: heroeImages,
    };
    return heroe;
  });
}

export {
  transformData,
}