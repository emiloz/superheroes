interface Heroe {
  id: number;
  name: string;
  powerstats: number;
  fullName: string;
  images: HeroeImages;
}

interface HeroeImages {
  md: string;
  sm: string;
}

export type {
  Heroe,
  HeroeImages,
}