import { Fragment, useState, useEffect } from 'react';

import search from '../assets/images/search.svg';

import '../assets/scss/heroesSearch.scss';

type SearchArgs = {
  setFilterHeroes: any;
}

export const HeroesSearch = ({ setFilterHeroes }: SearchArgs) => {

  const [ value, setValue ] = useState(' ');

  useEffect(() => {

  }, [value]);

  const handleChange = (value) => {
    setFilterHeroes(value);
    setValue(value)
  }

  return (
    <Fragment>
      <div className="container" style={{marginTop: '50px'}}>
        <div className="row">
          <div className="col-md-6">
            <h3>All Superheros</h3>
          </div>
          <div className="col-md-4 offset-md-2">
            <div className="input-group input-group-sm mb-3">
              <span className="input-group-text rounded-right search search__image">
                <img
                  src={search}
                  alt="search"
                  height="20"
                  width="20"
                />
              </span>
              <input
                type="text"
                className="form-control search"
                placeholder="Search"
                onChange={(e) => handleChange(e.target.value)}
                value={value}
              />
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}
