import React, { useState, useEffect } from "react";

import { CardHeroe } from './cardHeroe';

import { Heroe } from '../api/interfaces';

import heart from '../assets/images/medium-heart.svg';
import arrowUp from '../assets/images/arrow-up.svg';
import arrowDown from '../assets/images/arrow-down.svg';
import '../assets/scss/heroesLiked.scss';


type likedHeroeArg = {
  allLikedHeroes: Heroe[];
  deleteLikedHeroes: any;
};

const getSessionStorageOrDefault = (key: string) => {
  const isOpen = sessionStorage.getItem(key);
  if (isOpen === 'false') {
    return false;
  }
  return true;
}

export const HeroesLiked = ({ allLikedHeroes, deleteLikedHeroes }: likedHeroeArg) => {

  const [heroes, setHeroes] = useState<Heroe[]>([]);
  const [openLiked, setOpenLiked] = useState<boolean>(getSessionStorageOrDefault('openLiked'));

  useEffect(() => {
    sessionStorage.setItem('openLiked', (openLiked) ? openLiked.toString() : 'false');
    setHeroes(allLikedHeroes);
  }, [openLiked, allLikedHeroes]);

  const handleShowLiked = () => {
    setOpenLiked(!openLiked);
  }

  const heroeDeleteLikedHeroes = (id) => {
    deleteLikedHeroes(id);
  }

  return (
    <div className="container" style={{marginTop: '50px'}}>
      <div className={(openLiked) ? 'row liked-heroes' : 'row liked-heroes liked-heroes__open'}>
        <div className="col-12 liked-heroes__title">
          <div style={{ display: "flex" }} >
            <div className="liked-heroes__heart">
              <img
                src={heart}
                alt="heart"
                height="12"
                width="12"
              />
            </div>
            <h3 style={{ marginLeft: "10px" }}>Liked</h3>
          </div>
          <button
            className="liked-heroes__arrow"
            onClick={(e) => handleShowLiked()}
          >
            <img
              src={(openLiked) ? arrowDown: arrowUp}
              alt="arrowUp"
              height="12"
              width="12"
            />
          </button>
        </div>
        <div className="container">
          <div className="row" style={{textAlign: 'center'}}>
            {heroes && heroes.length === 0  && (
              <div>
                <img
                  src={heart}
                  alt="heart"
                  height="35"
                  width="35"
                  style={{marginBottom: '15px'}}
                />
                <h3>You haven’t liked any superhero yet</h3>
              </div>
            )}
            {heroes && heroes.map((heroe, key) => (
              <CardHeroe
                keyy={key}
                id={heroe.id}
                name={heroe.name}
                powerstats={heroe.powerstats}
                fullName={heroe.fullName}
                images={heroe.images}
                liked={true}
                itIsTheLast={heroes.length === (key + 1)}
                cardDeleteLikedHeroe={(e) => heroeDeleteLikedHeroes(e)}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
