import {
  Fragment,
  useState,
  useEffect,
} from 'react';
import axios from 'axios';

import { HeroesLiked } from './components/heroesLiked';
import { Heroes } from './components/heroes';
import { HeroesSearch } from './components/heroesSearch';
import { Loader } from './components/loaders';

import { Heroe } from './api/interfaces'

import { transformData } from './util/utils';

import logo from './assets/images/logo.svg';
import './assets/scss/app.scss';


let heroesLiked: Heroe[] = [];
const url = 'https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json';

const getSessionStorageLiked: any = () => {
  const heroesLiked = (sessionStorage.getItem('heroesLiked')) ? JSON.parse(sessionStorage.getItem('heroesLiked') || '') : '';
  console.log('heroesLiked', heroesLiked);
  return heroesLiked;
}


export const App = () => {

  const [isLoading, setIsLoading] = useState<Boolean>(false);
  const [heroes, setHeroes] = useState<Heroe[]>([]);
  const [likeds, setLikeds] = useState<any[]>(getSessionStorageLiked());
  const [likedHeroes, setLikedHeroes] = useState<number[]>([]);

  useEffect(() => {
    if (heroes.length === 0) {
      getHeroes();
    }
    setTimeout(() => {
      setIsLoading(true);
    }, 3000);
  });

  const getHeroes = async () => {
    const { data } = await axios.get(url);
    const allHeroes: Heroe[] = await transformData(data);
    setHeroes(allHeroes);
  }

  const setLiked = (id) => {
    likedHeroes.push(id);
    setLikedHeroes(likedHeroes);

    const heroesCopy = [...heroes];
    let heroesWithoutLiked: Heroe[] = [];

    heroesWithoutLiked = heroesCopy.filter((heroe) => {
      const { id } = heroe;
      const liked = likedHeroes.find((lh) => (lh === id));
      if (!liked) {
        return heroe
      }
    });

    heroesCopy.map((heroe) => {
      const { id } = heroe;
      const liked = likedHeroes.find((lh) => (lh === id));
      if (liked) {
        heroesLiked.push(heroe);
      }
    })

    let removeDuplicateLiked = heroesLiked.filter((hero) => {
      const { id } = hero;
      const isHeroeList = heroesWithoutLiked.findIndex((heroe) => (heroe.id === id));
      if (isHeroeList) {
        return hero;
      }
    });

    const removeDuplicateHeroesWithoutLiked = heroesWithoutLiked.filter((item,index)=>{
      return heroesWithoutLiked.indexOf(item) === index;
    });
    const removeDuplicateLikedHeroe = removeDuplicateLiked.filter((item,index)=>{
      return removeDuplicateLiked.indexOf(item) === index;
    });
    setHeroes(removeDuplicateHeroesWithoutLiked);
    sessionStorage.setItem('heroesLiked', JSON.stringify(removeDuplicateLikedHeroe));
    setLikeds(removeDuplicateLikedHeroe);
  }

  const deleteLiked = (id) => {
    const likedsCopy = [...likeds];

    const likedIndex = likedsCopy.findIndex((heroe) => (heroe.id === id));
    const likedDeleted = likedsCopy[likedIndex];

    likedsCopy.splice(likedIndex, 1);
    likedHeroes.splice(likedIndex, 1);

    sessionStorage.setItem('heroesLiked', JSON.stringify(likedsCopy));
    setLikeds(likedsCopy);
    setLikedHeroes(likedHeroes);

    const heroesCopy = [...heroes];
    heroesCopy.push(likedDeleted);
    heroesCopy.sort((a, b) => a.id - b.id);
    setHeroes(heroesCopy);
  }

  const filterHeroes = (value) =>  {
    let heroesCopy = [...heroes];
    let heroesFilter: Heroe[] = [];

    if (value !== '') {
      for(const heroe of heroesCopy) {
        const { name, fullName } = heroe;
        if (name.toLowerCase().indexOf(value.toLowerCase()) !== -1) {
          heroesFilter.push(heroe);
        }

        if (fullName.toLowerCase().indexOf(value.toLowerCase()) !== -1) {
          heroesFilter.push(heroe);
        }

        if (name.toLowerCase() === value.toLowerCase()) {
          heroesFilter.push(heroe);
        }
      }
      const removeDuplicateHeroesFilter = heroesFilter.filter((item,index)=>{
        return heroesFilter.indexOf(item) === index;
      });
      setHeroes(removeDuplicateHeroesFilter);
    } else {
      getHeroes();
    }
  }

  return (
    <Fragment>
      <header className="logo" style={{marginBottom: '100px'}}>
        <img
          src={logo}
          alt="logo"
          height="73"
          width="65"
        />
      </header>
      {!isLoading && <Loader />}
      {isLoading &&
        (<div>
            <HeroesLiked
              allLikedHeroes={likeds}
              deleteLikedHeroes={(e) => deleteLiked(e)}
            />
            <HeroesSearch setFilterHeroes={(e) => filterHeroes(e)} />
            <Heroes
              allHeroes={heroes}
              setLikedHeroes={(e) => setLiked(e)}
            />
          </div>
        )
      }
    </Fragment>
  );
}
